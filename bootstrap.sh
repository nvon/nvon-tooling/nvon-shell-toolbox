#!/bin/bash

scriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [ -z "$NWON_SHELL_TOOLBOX_LOCATION" ]; then
  echo "No value found for NWON_SHELL_TOOLBOX_LOCATION. Please set it in your environment." >/dev/stderr
  exit 1
fi

# Source all files in the utils folder and its subfolders
source "${NWON_SHELL_TOOLBOX_LOCATION}/utils/sourceAllShFiles.sh"
sourceAllShFiles "${NWON_SHELL_TOOLBOX_LOCATION}/utils"
