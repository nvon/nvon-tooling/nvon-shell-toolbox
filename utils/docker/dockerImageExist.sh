#!/bin/bash

dockerImageExists() {
  logInfo "Checking if ${1} image exists..."
  if [[ "$(docker images -q ${1} 2>/dev/null)" == "" ]]; then
    return 1
  else
    return 0
  fi
}
