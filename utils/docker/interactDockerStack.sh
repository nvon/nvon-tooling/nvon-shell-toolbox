#!/bin/bash

# ${@:2} means all but the first argument

# @param1 directory to look for docker-compose file
# @param* params to pass to docker-compose

startDockerStack() {
  dockerComposeCommand ${1} up -d ${@:2}
}

# @param1 directory to look for docker-compose file
# @param* params to pass to docker-compose

stopDockerStack() {
  dockerComposeCommand ${1} down ${@:2}
}

buildDockerStack() {
  startDockerStack ${1} --build --force-recreate
}
