#
# Return the home directory of a given user.
#
# @param1 the username. Defaults to the current user.
#
getHomeDirectoryForUser() {
  if [ -z $1 ]; then
    user=$USER
  else
    user=$1
  fi
  ensureUserExists $user

  echo $(getent passwd "$user" | cut -d: -f6)
}
