# NWON-Shell-Toolbox

Collection of bash functions for day-to-day usage.

## How to use

Right now, a lot of the functions depend on each other, so its best to use them all at once.
The toolbox expects a `NWON_SHELL_TOOLBOX_LOCATION` variable in your environment. 

First install:

```bash
cd [parent folder]
git clone git@gitlab.com:nvon/nvon-tooling/nvon-shell-toolbox.git
```

Then set in your .zshenv / .bashrc file:

```bash
NWON_SHELL_TOOLBOX_LOCATION=[parent folder]/nvon-shell-toolbox
source ${NWON_SHELL_TOOLBOX_LOCATION}/bootstrap.sh
```

Alternatively, to make your shell files more flexible, you can do something like:

```bash
# Put `export NWON_SHELL_TOOLBOX_LOCATION=path` in your .env!
source ~/.oh-my-zsh/.env
source ${NWON_SHELL_TOOLBOX_LOCATION}/bootstrap.sh
```
