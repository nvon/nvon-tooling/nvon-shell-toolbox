#!/bin/bash
set -euo pipefail

# install virtual environment handler
pip3 install virtualenv virtualenvwrapper

# create dir for virtual environemnts
mkdir ~/.virtualenvs

# install pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv

echo "

# Pyenv paths
export PYENV_ROOT=\"$HOME/.pyenv\"
export PATH=\"$PYENV_ROOT/bin:$PATH\"" >>~/.zshrc

# restart shell
exec "$SHELL"

# install pyenv-virtualenv
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv

echo "

# auto enter virtual environments
eval \"$(pyenv virtualenv-init -)\"" >>~/.zshenv

echo "

# Setup virtualenv home
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=~/.local/bin/virtualenv
source ~/.local/bin/virtualenvwrapper.sh

# Tell pyenv-virtualenvwrapper to use pyenv when creating new Python environments
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV=\"true\"

# Set the pyenv shims to initialize
if command -v pyenv 1>/dev/null 2>&1; then
 eval \"$(pyenv init -)\"
fi" >>~/.zshenv

# restart shell
exec "$SHELL"
